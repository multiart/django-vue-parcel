import Vue from 'vue'

window.LoadComponent = rootId => {
  console.log("!", rootId)
  const dataset = document.getElementById(rootId).dataset
  const data = dataset.data || '{}'
  const json = JSON.parse(data.replace(/'/g, '"'))
  const path = dataset.path
  var componentPath = ''

  console.log(json)
  const handleComponent = comp => {
    const Component = comp.default
    new Vue({
      render: h => h(Component, {props: json})
    }).$mount('#' + rootId)
  }
  switch(path) {
    case 'home':
     import('./../src/components/Home')
      .then(handleComponent)
     break;
  }
}
