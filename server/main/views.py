from django.views import generic


class AboutView(generic.TemplateView):
    template_name = 'about.html'


class ContactView(generic.TemplateView):
    template_name = 'contact-us.html'


class HomeView(generic.TemplateView):
    template_name = 'home.html'
